FROM node:19-alpine

MAINTAINER Brain.js

RUN apk update && \
    apk add --no-cache \
    alpine-sdk \
    glew-dev \
    libxi-dev \
    pkgconf \
    python3

RUN ln -s /usr/bin/python3 /usr/bin/python

WORKDIR /app

COPY package*.json tsconfig.json ./

RUN npm install

COPY src ./src

RUN npm run build

RUN chown -R node /app && chmod -R 740 /app

USER node

ENV NODE_ENV=production

CMD ["npm", "start"]
