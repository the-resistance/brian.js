import cluster from 'cluster'
import cpuCount from 'os'
import { pipeline } from './stages.js'

if (cluster.isPrimary) masterProcess()
else childProcess()

function masterProcess() {
    console.log(`Master process ${process.pid} is running`)

    // for (let i = 0; i < cpuCount.cpus().length; i++) {
    //     console.log(`Forking process number ${i}...`)

    //     cluster.fork() //creates new node js processes
    // }
    console.log(cpuCount.cpus().length)
    cluster.fork()

    cluster.on('exit', (worker, code, signal) => {
        console.log(`worker ${worker.process.pid} died`)

        cluster.fork() //forks a new process if any process dies
    })
}

function childProcess() {
    pipeline()
}
