import fs from 'fs'
import brain from 'brain.js'
import { IRNNDatum, Value } from 'brain.js/dist/src/recurrent/rnn-data-types'

const net = new brain.recurrent.GRU({
    hiddenLayers: [10],
    inputSize: 2,
    maxPredictionLength: 500,
    outputSize: 2
})

export function trainModel() {
    const data: (IRNNDatum | Value)[] = [
        {
            input: 'Brian',
            output: 'bafkreigsjph4cxrotdctrhfzo7h3wwugv2cy7ro6xd2h4f4fabgtixtiba'
        },
        {
            input: 'Brain',
            output: 'bafkreibryxd4ssx57jtiabdtxrpwtl7duvfdvrtrkujqz5u6myhhlalg6u'
        }
    ]

    net.train(data, {
        errorThresh: 0.05,
        iterations: 500000,
        timeout: Infinity,
        learningRate: 0.01,
        log: true,
        logPeriod: 1
    })

    fs.writeFileSync('./data/out.json', JSON.stringify(net.toJSON()))
}

// break book into sentences
// array of sentences goes into the timestep
// feed each sentence, one by one
// ingest new stuff, one by one
// append to sentences timestep
