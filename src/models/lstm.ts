import fs from 'fs'
import brain from 'brain.js'
import natural from 'natural'
import { IRNNDatum, Value } from 'brain.js/dist/src/recurrent/rnn-data-types'

// natural.LancasterStemmer.attach()

export function trainModel() {
    const tokenizer = new natural.SentenceTokenizer()
    const NGrams = natural.NGrams

    const net = new brain.recurrent.LSTM({
        hiddenLayers: [10, 10, 10],
        inputSize: 2,
        maxPredictionLength: 500,
        outputSize: 2
    })

    const paragraph = `God told me that the first stage of Quantum Awakening is waking up to the lies of the scum (evil), and interrelated with that awakening, is the awakening to Jesus, because the world does not know Jesus, so to know Him, is to have the faith that overcomes the lies of the machine. The "evil" in this world is actually the illusion of the absence of perfection. To know Him, is to get out of the world. So, when asked, "What would you call the daily suffering in the world?" I would respond "Lack of Faith." We can all attest that we've been here before and done this part. The last cycle the Enchantress wasn't nice, this time she is somewhat cornered. Time is supreme, Time is a mighty pow'r, whom wisest mortals will adore. How happy could I linger here, And stop old Time in his career! Faithful mirror, fair-reflecting, All my beauteous charms collecting, Which, I fear, will soon decay. Thou shalt flourish in thy splendour, While these glories I surrender, Horrid Time's devoted prey. Faithful mirror... da capo. Can you master interpreting memes here into pure images to not experience miniscule guilt and shame? How (you) would shape an antimeme? Consider as you are reviewing, see how they fit, now that you have a grip on your unique thought constructs, you can use what new perspectives you have been given to complete these. Christianity has recognized this parallel in the equation God is love. God as a person—or three—is equated to love, an interaction. In the language of causing existence anthropology, God is not merely the central object-victim that brings the human community together, but the interrelationship mediated by this object between the members of this community. When we truly love each other in God, he no longer occupies a “center” that distracts us from each other. Yet to say that God is love is not merely to say, “the center is the periphery,” but to recognize that without the center the periphery would never have existed.`
    const sentences = tokenizer.tokenize(paragraph)

    console.log(sentences)

    const pairs = sentences.map((sentence: string) => {
        return { input: NGrams.trigrams(sentence).flat(), output: sentence }
    })

    console.log(pairs)
    const data: (IRNNDatum | Value)[] = pairs

    console.log('Training...')
    net.train(data, {
        errorThresh: 0.05,
        iterations: 20000,
        timeout: Infinity,
        learningRate: 0.3,
        log: true,
        logPeriod: 1
    })

    fs.writeFileSync('./data/out.json', JSON.stringify(net.toJSON()))
    console.log(net.run('Who is God?'))
    console.log(net.run('Tell me about the Enchantress.'))
    console.log(net.run('Are you evil?'))
}

// break book into sentences
// array of sentences goes into the timestep
// feed each sentence, one by one
// ingest new stuff, one by one
// append to sentences timestep
